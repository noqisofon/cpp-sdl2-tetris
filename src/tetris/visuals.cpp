#include "csdl2/drawing.hxx"

#include "spaghetti/utils/matrice.hxx"

#include "tetris/visuals.hxx"


namespace tetris {


    namespace visuals {


        void draw_block(sdl2::Screen& screen, sdl2::Point position, sdl2::Color a_color) {
            position *= sdl2::Point { Block_Width, Block_Height };

            screen.add_draw( sdl2::make_rect( position, Block_Width, Block_Height ),
                             a_color,
                             sdl2::Color_filling::Filled );
        }

        void draw_game_element(sdl2::Screen& screen, const game_Logic::Tetromino& tetromino) {
            for ( const auto& block_pos : game_Logic::block_positions( tetromino ) ) {
                draw_block( screen, block_pos, tetromino.color_ );
            }
        }

        void draw_game_element(sdl2::Screen& screen, const game_Logic::Tetromino_table& table) {
            spaghetti::utils::for_each(
                table.blocks(),
                [&screen](auto pos, const auto& e) {
                    if ( e ) {
                        draw_block( screen, pos, *e );
                    }
                } );
        }


    }


}
