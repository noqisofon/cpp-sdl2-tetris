#include "tetris/game_logic.hxx"

#include "tetris/game.hxx"


namespace tetris {


    namespace game_Logic {


        Game_timer::Game_timer(std::chrono::milliseconds delay) noexcept
            : delay_ {delay}, timer_ {delay}
        {
        }

        bool Game_timer::ready() noexcept {
            return timer_.ready();
        }

        void Game_timer::set_mode(Drop_mode mode) noexcept {
            if ( mode == Drop_mode::Normal ) {
                timer_.set_delay( delay_ );
            } else if ( mode == Drop_mode::Fast ) {
                timer_.set_delay( min() );
            }
        }

        void Game_timer::speed_up() noexcept {
            if ( delay_ > min() ) {
                delay_ -= speed_boost();
            }
        }

        Button_dispatcher::Button_dispatcher()
            : keydown_dispatch_ {
            { SDL_SCANCODE_LEFT, [](auto dispatcher_data) noexcept {
                                     safe_move( dispatcher_data.tetromino_, dispatcher_data.table_, { -1, 0 } );
                                 }
            },
            { SDL_SCANCODE_RIGHT, [](auto dispatcher_data) noexcept {
                                      safe_move( dispatcher_data.tetromino_, dispatcher_data.table_, { 1, 0 } );
                                  }
            },
            { SDL_SCANCODE_X, [](auto dispatcher_data) noexcept {
                                  safe_rotate( dispatcher_data.tetromino_,
                                               dispatcher_data.table_,
                                               Rotation::Clockwise );
                              }
            },
            { SDL_SCANCODE_Y, [](auto dispatcher_data) noexcept {
                                  safe_rotate( dispatcher_data.tetromino_,
                                               dispatcher_data.table_,
                                               Rotation::Counter_Clockwise );
                              }
            },
            { SDL_SCANCODE_DOWN, [](auto dispatcher_data) noexcept {
                                     dispatcher_data.game_timer_.set_mode( Drop_mode::Fast );
                                 }
            }
        }, keyup_dispatch_ {
            { SDL_SCANCODE_DOWN, [](auto dispatcher_data) noexcept {
                                     dispatcher_data.game_timer_.set_mode( Drop_mode::Normal );
                                 }
            }
            
        }
        {
        }

        void Button_dispatcher::operator ()(const sdl2::Event& an_event, Button_dispatcher_data data) noexcept {
            switch ( an_event.type ) {
            case SDL_KEYDOWN:
                do_dispatch( keydown_dispatch_,
                             data,
                             an_event.key.keysym.scancode );
                break;

            case SDL_KEYUP:
                do_dispatch( keyup_dispatch_,
                             data,
                             an_event.key.keysym.scancode );
                break;
            } 
        }

        void Button_dispatcher::do_dispatch(Dispatch_map&          dispatch_map,
                                            Button_dispatcher_data data,
                                            const sdl2::Scancode&  scancode) noexcept {
            if ( dispatch_map.count( scancode ) == 0 ) {

                return ;
            }

            dispatch_map[scancode]( data );
        }

        Game::Game(std::chrono::milliseconds delay) noexcept
            : timer_ { delay }
        {
        }

        void Game::frame_advance(const sdl2::Event& an_event) {
            handle_event( an_event );
        }

        void Game::frame_advance(sdl2::Screen& screen) {
            check_timer();

            visuals::draw_game_element( screen, current_tetromino_ );
            visuals::draw_game_element( screen, table_ );
        }

        void Game::check_timer() noexcept {
            if ( game_over_ ) {

                return ;
            }

            if ( !timer_.ready() ) {

                return ;
            }

            if ( touches_ground( current_tetromino_ ) ||
                 touches_table( table_, current_tetromino_, { 0, 1 } ) ) {
                insert_tetromino();
                handle_rows();
                reset();
                check_game_over();

                return ;
            }

            current_tetromino_.pos_ += sdl2::Point { 0, 1 };
        }

        void Game::handle_event(const sdl2::Event& an_event) {
            dispatch_button_( an_event, { current_tetromino_, table_, timer_ } );
        }

        void Game::insert_tetromino() noexcept {
            table_.insert( current_tetromino_ );
        }

        void Game::reset() noexcept {
            current_tetromino_ = create_tetromino();
        }

        void Game::check_game_over() noexcept {
            if ( table_overlaps_tetromino( table_, current_tetromino_ ) ) {
                game_over_ = true;

                sdl2::Message_content content {};
                content.title = "Game over!";
                content.text  = "Final score: " + std::to_string( score_ ) + ".\n";

                sdl2::show_message( content, sdl2::Message_box_type::Basic );
            }
        }

        void Game::handle_rows() noexcept {
            auto cleared_rows = table_.clear_rows();

            score_ += cleared_rows;

            for ( std::int32_t _ = 0; _ < cleared_rows; ++ _ ) {
                timer_.speed_up();
            }
        }

        Tetromino Game::create_tetromino() noexcept {
            return factory_.create( { Table_Width / 2, 0 } );
        }


    }


}
