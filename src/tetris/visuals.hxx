#pragma once

#include <cstddef>

#include "csdl2/drawing.hxx"

#include "tetris/game_logic.hxx"


namespace tetris {


    namespace visuals {


        /*!
         * ブロックの幅(pixel)
         */
        constexpr auto Block_Width  = 20;
        /*!
         * ブロックの高さ(pixel)
         */
        constexpr auto Block_Height = 20;

        constexpr std::size_t Window_Width  = Block_Width * game_Logic::Table_Width;
        constexpr std::size_t Window_Height = Block_Height * game_Logic::Table_Height;

        /*!
         *
         */
        void draw_block(sdl2::Screen&, sdl2::Point, sdl2::Color);

        /*!
         *
         */
        void draw_game_element(sdl2::Screen&, const game_Logic::Tetromino&);
        /*!
         *
         */
        void draw_game_element(sdl2::Screen&, const game_Logic::Tetromino_table&);


    }


}
