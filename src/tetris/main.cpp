#include "tetris/game.hxx"
#include "tetris/game_logic.hxx"

int main() {
    using namespace sdl2;
    using namespace tetris::game_Logic;
    using namespace tetris::visuals;
    using namespace std::literals::chrono_literals;

    Sdl_system_control _ {};

    Screen_properties  props {};
    // props.width  = Window_Width;
    props.width  = 800;
    // props.height = Window_Height;
    props.height = 600;
    props.title  = "SDL2 Tetris";

    Screen screen {props};

    Game   tetris {500ms};

    Event_loop loop {
        [&screen, &tetris](const auto an_event) {
            if ( an_event.type == SDL_QUIT ) {

                return Event_result::Quit;
            }

            tetris.frame_advance( an_event );

            return Event_result::Continue;
        },
        [&screen, &tetris] {
            tetris.frame_advance( screen );
            screen.redraw( color_white() );
        } };

    loop.start();

    return 0;
}
