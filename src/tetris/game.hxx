#pragma once

#include <functional>
#include <map>
#include <chrono>

#include "tetris/game_logic.hxx"
#include "tetris/visuals.hxx"


namespace tetris {


    namespace game_Logic {


        enum class Drop_mode : bool { Normal, Fast };


        class Game_timer {
         public:
            explicit Game_timer(std::chrono::milliseconds) noexcept;

            static constexpr std::chrono::milliseconds min() noexcept {
                using namespace std::chrono_literals;

                return 50ms;
            }

            static constexpr std::chrono::milliseconds speed_boost() noexcept {
                using namespace std::chrono_literals;

                return 10ms;
            }

            bool ready() noexcept;

            void set_mode(Drop_mode) noexcept;

            void speed_up() noexcept;

         private:
            std::chrono::milliseconds delay_;
            sdl2::Timer               timer_;
        };

        struct Button_dispatcher_data;

        /*!
         *
         */
        class Button_dispatcher {
         public:
            Button_dispatcher();

            void operator ()(const sdl2::Event&, Button_dispatcher_data) noexcept;

         private:
            using Dispatch_function =
                std::function<void(Button_dispatcher_data) noexcept>;

            using Dispatch_map = std::map<sdl2::Scancode, Dispatch_function>;

            void do_dispatch(Dispatch_map&,
                             Button_dispatcher_data,
                             const sdl2::Scancode&) noexcept;

            Dispatch_map keydown_dispatch_;
            Dispatch_map keyup_dispatch_;
        };

        /*!
         *
         */
        struct Button_dispatcher_data {
            Tetromino&                  tetromino_;
            const Tetromino_table&      table_;

            Game_timer&                 game_timer_;
        };

        /*!
         *
         */
        class Game {
         public:
            explicit Game(std::chrono::milliseconds) noexcept;

            void frame_advance(const sdl2::Event&);
            void frame_advance(sdl2::Screen&);

         private:
            void check_timer() noexcept;

            void handle_event(const sdl2::Event&);

            void insert_tetromino() noexcept;

            void reset() noexcept;

            void check_game_over() noexcept;

            void handle_rows() noexcept;

            Tetromino create_tetromino() noexcept;

            Game_timer        timer_;
            Button_dispatcher dispatch_button_ {};
            Tetromino_factory factory_ {};
            Tetromino_table   table_ {};
            Tetromino         current_tetromino_ { create_tetromino() };
            std::int32_t      score_ {0};
            bool              game_over_ {false};
        };
        

    }


}
