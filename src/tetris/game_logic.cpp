#include "game_logic.hxx"

#include <utility>
#include <algorithm>
#include <iterator>

namespace tetris {


    namespace game_Logic {


        namespace {

            using Point_vector_iterator = sdl2::Point_vector::const_iterator;

            using Predicate_f           = std::function<bool(sdl2::Point)>;

            using Outer_f               = std::function<bool(Point_vector_iterator, Point_vector_iterator, Predicate_f)>;

            bool apply_to_points(Outer_f          outer,
                                 const Tetromino& tetromino,
                                 Predicate_f      inner) {
                auto positions = block_positions( tetromino );

                return outer( positions.cbegin(), positions.cend(), inner );
            }

            bool any_of_points(const Tetromino& tetromino, Predicate_f pred) {
                return apply_to_points( std::any_of<Point_vector_iterator, Predicate_f>,
                                        tetromino,
                                        pred );
            }

            bool all_of_points(const Tetromino& tetromino, Predicate_f pred) {
                return apply_to_points( std::all_of<Point_vector_iterator, Predicate_f>,
                                        tetromino,
                                        pred );
            }
        }

        Tetromino_block_offsets::Tetromino_block_offsets(const Blocks& blocks) noexcept
            : blocks_{&blocks} {
        }

        const sdl2::Point_vector& Tetromino_block_offsets::get() const noexcept {
            return blocks_->at( angle_ );
        }

        void Tetromino_block_offsets::rotate(Rotation a_rotation) noexcept {
            angle_ = rotate_angle( angle_, a_rotation );
        }

        Angle rotate_angle(Angle an_angle, Rotation a_rotation) noexcept {
            auto angle_raw = static_cast<std::int32_t>( an_angle );

            angle_raw += ( a_rotation == Rotation::Clockwise ) ? -90 : 90;

            return static_cast<Angle>( first_quadrant( angle_raw ) );
        }

        std::int32_t first_quadrant(std::int32_t angle_raw) noexcept {
            if ( angle_raw == -90 ) {

                return 270;
            }

            if ( angle_raw == 360 ) {

                return 0;
            }

            return angle_raw;
        }

        sdl2::Point_vector block_positions(const Tetromino& tetromino) {
            auto results = tetromino.block_offsets_.get();

            for ( auto& result : results ) {
                result += tetromino.pos_;
            }

            return results;
        }

        void safe_move(Tetromino&             tetromino,
                       const Tetromino_table& table,
                       sdl2::Point            move_by) noexcept {
            tetromino.pos_ += move_by;

            if ( !on_screen( tetromino ) || touches_table( table, tetromino, {} ) ) {
                tetromino.pos_ -= move_by;
            }
        }

        void safe_rotate(Tetromino&             tetromino,
                         const Tetromino_table& table,
                         Rotation               a_rotation) noexcept {
            auto new_tetromino = tetromino;

            new_tetromino.block_offsets_.rotate( a_rotation );
            if ( on_screen( new_tetromino ) &&
                 !table_overlaps_tetromino( table, new_tetromino ) ) {
                tetromino = std::move( new_tetromino );
            }
        }

        void Tetromino_table::insert(const Tetromino& tetromino) noexcept {
            for ( const auto& block_pos : block_positions( tetromino ) ) {
                if ( on_screen( block_pos ) ) {
                    spaghetti::utils::at( blocks_, block_pos ) = tetromino.color_;
                }
            }
        }

        std::int32_t Tetromino_table::clear_rows() noexcept {
            auto rows_cleared = clear_and_count_rows();

            if ( rows_cleared > 0 ) {
                apply_gravity();
            }

            return rows_cleared;
        }

        auto Tetromino_table::blocks() const noexcept -> const Matrice& {
            return blocks_;
        }

        void Tetromino_table::clear_row(Row_type& row) noexcept {
            for ( auto& block : row ) {
                block = boost::none;
            }
        }

        void Tetromino_table::apply_gravity() noexcept {
            spaghetti::utils::for_each_row(
                blocks_,
                [this](auto n, auto& row) {
                    if ( n == 0 || !empty( spaghetti::utils::row( this->blocks_, n - 1 ) ) ) {

                        return ;
                    }
                    this->shift_row( n - 1 );
                    this->clear_row( spaghetti::utils::row( this->blocks_, n - 1 ) );
                    this->apply_gravity();
                } );
        }

        void Tetromino_table::shift_row(std::size_t n) noexcept {
            spaghetti::utils::row( blocks_, n + 1 ) = spaghetti::utils::row( blocks_, n );
        }

        std::int32_t Tetromino_table::clear_and_count_rows() noexcept {
            std::int32_t counter = 0;

            spaghetti::utils::for_each_row(
                blocks_,
                [this, &counter](auto, auto& row) {
                    if ( filled( row ) ) {
                        ++ counter;
                        this->clear_row( row );
                    }
                } );

            return counter;
        }

        auto block_exists_lambda() noexcept {
            return [](const auto& block) { return block ? true : false; };
        }

        bool filled(const Tetromino_table::Row_type& row) noexcept {
            return std::all_of( row.cbegin(), row.cend(), block_exists_lambda() );
        }

        bool empty(const Tetromino_table::Row_type& row) noexcept {
            return std::none_of( row.cbegin(), row.cend(), block_exists_lambda() );
        }

        bool touches_table(const Tetromino_table& table,
                           const Tetromino&       tetromino,
                           sdl2::Point            relative) noexcept {
            return any_of_points(
                tetromino,
                [&table, relative](const auto& pos) {
                    return table_has_point( table, pos + relative );
                } );
        }

        bool table_has_point(const Tetromino_table& table,
                             sdl2::Point            point) noexcept {
            bool found = false;

            spaghetti::utils::for_each(
                table.blocks(),
                [point, &found](auto pos, const auto& oc) -> void {
                    if ( found || !oc ) {

                        return ;
                    }

                    if ( pos == point ) {
                        found = true;
                    }
                } );

            return found;
        }

        bool table_overlaps_tetromino(const Tetromino_table& table,
                                      const Tetromino&       tetromino) noexcept {
            return touches_table( table, tetromino, { 0, 0 } );
        }

        bool overflown(const Tetromino_table& table) noexcept {
            const auto & top_row = spaghetti::utils::row( table.blocks(), 0 );

            return std::any_of(
                top_row.cbegin(), top_row.cend(),
                [](const auto& e) {
                    return e ? true : false;
                } );
        }

        bool on_screen(sdl2::Point a_point) noexcept {
            return a_point.x >= 0 && static_cast<std::size_t>( a_point.x ) < Table_Width;
        }

        bool on_screen(const Tetromino& tetromino) noexcept {
            return all_of_points(
                tetromino,
                [](const auto& pos) { return on_screen( pos ); } );
        }

        bool touches_ground(const Tetromino& tetromino) noexcept {
            return any_of_points(
                tetromino,
                [](const auto& pos) { return pos.y == Table_Height - 1; } );
        }

        Tetromino_factory::Tetromino_factory() {
            add_block_proto( Tetromino_type::I,
                             Angle::Zero,
                             {{-2, 0}, {-1, 0}, {0, 0}, {1, 0}} );

            add_block_proto( Tetromino_type::I,
                             Angle::Ninety,
                             {{0, -2}, {0, -1}, {0, 0}, {0, 1}} );

            add_block_proto( Tetromino_type::O,
                             Angle::Zero,
                             {{0, 1}, {1, 0}, {0, 0}, {1, 1}} );

            add_block_proto( Tetromino_type::S,
                             Angle::Zero,
                             {{-1, 1}, {0, 1}, {0, 0}, {1, 0}} );

            add_block_proto( Tetromino_type::S,
                             Angle::Ninety,
                             {{0, -1}, {0, 0}, {1, 0}, {1, 1}} );

            add_block_proto( Tetromino_type::T,
                             Angle::Zero,
                             {{0, 1}, {0, 0}, {-1, 0}, {1, 0}} );

            add_block_proto( Tetromino_type::T,
                             Angle::Ninety,
                             {{0, -1}, {-1, 0}, {0, 1}, {0, 0}} );

            add_block_proto( Tetromino_type::T,
                             Angle::One_Eighty,
                             {{1, 0}, {0, 0}, {-1, 0}, {0, 0}} );

            add_block_proto( Tetromino_type::T,
                             Angle::Two_Seventy,
                             {{0, -1}, {0, 0}, {0, 1}, {1, 0}} );

            add_block_proto( Tetromino_type::Z,
                             Angle::Zero,
                             {{-1, 0}, {0, 0}, {0, 1}, {-1, 1}} );

            add_block_proto( Tetromino_type::Z,
                             Angle::Ninety,
                             {{0, 1}, {0, 0}, {1, 0}, {1, -1}} );

            add_block_proto( Tetromino_type::J,
                             Angle::Zero,
                             {{-1, 0}, {0, 0}, {1, 0}, {1, 1}} );

            add_block_proto( Tetromino_type::J,
                             Angle::One_Eighty,
                             {{-1, -1}, {0, 0}, {-1, 0}, {1, 0}} );

            add_block_proto( Tetromino_type::J,
                             Angle::Two_Seventy,
                             {{-1, 1}, {0, 1}, {0, 0}, {0, -1}} );

            add_block_proto( Tetromino_type::L,
                             Angle::Zero,
                             {{-1, 0}, {-1, 1}, {0, 0}, {1, 0}} );

            add_block_proto( Tetromino_type::L,
                             Angle::Ninety,
                             {{0, -1}, {0, 0}, {0, 1}, {1, 1}} );

            add_block_proto( Tetromino_type::L,
                             Angle::One_Eighty,
                             {{0, -1}, {0, 0}, {1, 0}, {1, -1}} );

            add_block_proto( Tetromino_type::L,
                             Angle::One_Eighty,
                             {{-1, -1}, {0, -1}, {0, 0}, {0, -1}} );

            two_versions( Tetromino_type::I );
            two_versions( Tetromino_type::O );
            two_versions( Tetromino_type::S );
            two_versions( Tetromino_type::Z );
        }

        Tetromino Tetromino_factory::create(sdl2::Point pos) {
            return { pos, Tetromino_block_offsets{ random_blocks() }, random_color() };
        }

        const Tetromino_block_offsets::Blocks& Tetromino_factory::random_blocks() const noexcept {
            auto max = static_cast<std::int32_t>( Tetromino_type::S );

            std::uniform_int_distribution<std::int32_t> distribution( 0, max );

            return block_protos_.at( static_cast<Tetromino_type>( distribution( random_device_ ) ) );
        }

        sdl2::Color Tetromino_factory::random_color() const noexcept {
            std::uniform_int_distribution<std::size_t> distribution( 0,
                                                                     color_protos_.size() - 1 );

            return color_protos_[distribution( random_device_ )];
        }

        void Tetromino_factory::two_versions(Tetromino_type type) {
            auto& blocks = block_protos_[type];

            blocks[Angle::One_Eighty]  = blocks[Angle::Zero];
            blocks[Angle::Two_Seventy] = blocks[Angle::Ninety];
        }

        void Tetromino_factory::one_versions(Tetromino_type type) {
            auto& blocks = block_protos_[type];

            blocks[Angle::Ninety] = blocks[Angle::One_Eighty] =
                blocks[Angle::Two_Seventy] = blocks[Angle::Zero];
        }

        void Tetromino_factory::add_block_proto(Tetromino_type type, Angle an_angle, sdl2::Point_vector points) noexcept {
            block_protos_[type][an_angle] = std::move( points );
        }


    }


}
