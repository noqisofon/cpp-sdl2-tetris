#pragma once

#include <cstddef>
#include <cmath>

#include <array>
#include <vector>
#include <unordered_map>
#include <random>
#include <chrono>
#include <utility>

#include <iostream>

#include <boost/optional/optional.hpp>

#include "spaghetti/utils/matrice.hxx"

#include "csdl2/drawing.hxx"
#include "csdl2/flow.hxx"


namespace tetris {


    namespace game_Logic {


        using score_t = int;

        /*!
         * Rotation はテトロミノがどっち向きに回るかを表します。
         */
        enum class Rotation : bool {
            Clockwise,
            Counter_Clockwise
        };

        enum class Angle {
            Zero        =   0,
            Ninety      =  90,
            One_Eighty  = 180,
            Two_Seventy = 270
        };

        /*!
         * 
         */
        class Tetromino_block_offsets {
         public:
            using Blocks =  std::unordered_map<Angle, sdl2::Point_vector>;

            explicit Tetromino_block_offsets(const Blocks&) noexcept;

            const sdl2::Point_vector& get() const noexcept;

            void                      rotate(Rotation) noexcept;

            void                      clear() noexcept;

         private:
            const Blocks       *blocks_ {};
            sdl2::Point_vector  no_points_{};
            Angle               angle_{Angle::Zero};
        };


        /*!
         *
         */
        Angle rotate_angle(Angle, Rotation) noexcept;

        /*!
         *
         */
        std::int32_t first_quadrant(std::int32_t) noexcept;

        /*!
         * テトロミノを表します。
         */
        struct Tetromino {
            Tetromino(const Tetromino&) = default;
            Tetromino(Tetromino&&)      = default;

            Tetromino& operator = (const Tetromino&) = default;
            Tetromino& operator = (Tetromino&&)      = default;

            sdl2::Point              pos_;
            Tetromino_block_offsets  block_offsets_;
            sdl2::Color              color_;
        };

        /*!
         * 
         */
        sdl2::Point_vector block_positions(const Tetromino&);

        using Optional_color = boost::optional<sdl2::Color>;

        constexpr std::size_t Table_Width     =  8;
        constexpr std::size_t Table_Height    = 16;
        constexpr std::size_t Tetromino_Count =  7;

        /*!
         * 全てのテトロミノブロックが入っています。
         */
        class Tetromino_table {
         public:
            using Matrice  = spaghetti::utils::Matrice<Optional_color, Table_Width, Table_Height>;
            using Row_type = Matrice::value_type;

            /*!
             *
             */
            void insert(const Tetromino&) noexcept;

            /*!
             *
             */
            std::int32_t clear_rows() noexcept;

            /*!
             *
             */
            const Matrice& blocks() const noexcept;

         private:
            /*!
             *
             */
            void clear_row(Row_type&) noexcept;

            /*!
             *
             */
            void apply_gravity() noexcept;

            /*!
             *
             */
            void shift_row(std::size_t) noexcept;

            /*!
             *
             */
            std::int32_t clear_and_count_rows() noexcept;

            Matrice blocks_{};
        };

        /*!
         *
         */
        void safe_move(Tetromino&, const Tetromino_table&, sdl2::Point) noexcept;

        /*!
         *
         */
        void safe_rotate(Tetromino&, const Tetromino_table&, Rotation) noexcept;

        /*!
         *
         */
        bool filled(const Tetromino_table::Row_type&) noexcept;

        /*!
         *
         */
        bool empty(const Tetromino_table::Row_type&) noexcept;

        /*!
         *
         */
        bool touches_table(const Tetromino_table&,
                           const Tetromino&,
                           sdl2::Point) noexcept;

        /*!
         *
         */
        bool table_has_point(const Tetromino_table&, sdl2::Point) noexcept;

        /*!
         *
         */
        bool table_overlaps_tetromino(const Tetromino_table&,
                                      const Tetromino&) noexcept;

        /*!
         *
         */
        bool overflown(const Tetromino_table&) noexcept;

        /*!
         *
         */
        bool on_screen(sdl2::Point) noexcept;
        /*!
         *
         */
        bool on_screen(const Tetromino&) noexcept;

        /*!
         *
         */
        bool touches_ground(const Tetromino&) noexcept;

        /*!
         *
         */
        enum class Tetromino_type { I, O, T, J, L, S, Z };

        /*!
         *
         */
        class Tetromino_factory {
         public:
            /*!
             *
             */
            Tetromino_factory();

            /*!
             *
             */
            Tetromino create(sdl2::Point);

         private:
            /*!
             *
             */
            const Tetromino_block_offsets::Blocks& random_blocks() const noexcept;

            /*!
             *
             */
            sdl2::Color random_color() const noexcept;

            /*!
             *
             */
            void one_versions(Tetromino_type);

            /*!
             *
             */
            void two_versions(Tetromino_type);

            /*!
             *
             */
            void add_block_proto(Tetromino_type, Angle, sdl2::Point_vector) noexcept;

            using Block_prototypes = std::unordered_map<Tetromino_type, Tetromino_block_offsets::Blocks>;

            Block_prototypes block_protos_{};

            std::vector<sdl2::Color> color_protos_{ sdl2::color_red(),
                                                    sdl2::color_blue(),
                                                    sdl2::color_green(),
                                                    sdl2::color_yellow() };

            mutable std::random_device random_device_{};
        };

        
    }


}
