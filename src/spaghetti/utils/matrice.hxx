#pragma once

#include <cstddef>

#include <functional>
#include <utility>

#include <array>

#include "csdl2/drawing.hxx"


namespace spaghetti {


    namespace utils {


        template <class _Type,
                  std::size_t _Width,
                  std::size_t _Height>
        using Matrice = std::array<std::array<_Type, _Width>, _Height>;

        /*!
         *
         */
        template <class _Type>
        using Value_type = typename _Type::value_type;

        /*!
         *
         */
        template <class, bool _Is_Const>
        struct Pres_value_type;

        template <class _From>
        struct Pres_value_type<_From, true> {
            using value_type = const Value_type<_From>;
        };

        template <class _From>
        struct Pres_value_type<_From, false> {
            using value_type = Value_type<_From>;
        };

        /*!
         *
         */
        template <class _From>
        using Pres_which_value_type =
            typename Pres_value_type<_From, std::is_const<_From>::value>::value_type;

        /*!
         *
         */
        template <class _Matrice>
        auto& row(_Matrice& matrice, std::size_t n) {
            return matrice.at( n );
        }

        /*!
         *
         */
        template <class _Matrice>
        auto& at(_Matrice& matrice, sdl2::Point pos) {
            return matrice.at( pos.y ).at( pos.x );
        }

        /*!
         *
         */
        template <class _Matrice>
        void for_each_row(_Matrice& matrice,
                          std::function<void(std::size_t,
                                             Pres_which_value_type<_Matrice>&)> callback) {
            for ( std::size_t y = 0; y < matrice.size(); ++ y ) {
                callback( y, row( matrice, y ) );
            }
        }

        /*!
         *
         */
        template <class _Matrice>
        void for_each(_Matrice& matrice, std::function<void(sdl2::Point,
                                                            Pres_which_value_type<Pres_which_value_type<_Matrice>>&)> callback) {
            for_each_row(
                matrice,
                [callback](auto y, auto& row) {
                    for ( std::size_t x = 0; x < row.size(); ++ x ) {
                        callback( { static_cast<std::int32_t>( x ), static_cast<std::int32_t>( y ) }, row[x] );
                    }
                } );
        }


    }


}
