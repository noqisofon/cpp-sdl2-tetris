#include "csdl2/drawing.hxx"

#define CHECK_FUNCTION(_function_name_, ...)    check_function( _function_name_( __VA_ARGS__ ) )


namespace sdl2 {

    
    Rect make_rect(std::int32_t x, std::int32_t y, std::uint32_t width, std::uint32_t height) noexcept {
        return { x, y, width, height };
    }

    Rect make_rect(Point pos, std::uint32_t width, std::uint32_t height) noexcept {
        return { pos.x, pos.y, width, height };
    }

    Rect make_rect(Point pos, Size size) noexcept {
        return { pos.x, pos.y, size.width, size.height };
    }

    void Texture_factory::add(const std::string& name, Unique_renderer& renderer) {
        Drawable_texture drawable = { load_texture( name.c_str(), renderer ) };

        CHECK_FUNCTION( SDL_QueryTexture,
                        drawable.texture.get(),
                        nullptr, nullptr,
                        &drawable.width, &drawable.height );

        texture_map_[name] = std::move( drawable );
    }

    bool Texture_factory::exists(const std::string& name) const noexcept {
        return texture_map_.count( name ) != 0;
    }

    Drawable_texture Texture_factory::get(const std::string& name) const noexcept {
        return texture_map_.at( name );
    }

    Screen::Screen(const Screen_properties& properties)
        : canvas_ {properties}
    {
        CHECK_FUNCTION( SDL_SetRenderDrawBlendMode,
                        canvas_.renderer.get(),
                        SDL_BLENDMODE_BLEND );
    }

    void Screen::add_draw(const std::string& name, Point where) {
        if ( factory_.exists( name ) ) {
            factory_.add( name, canvas_.renderer );
        }

        snapshots_.push_back(
            [this, name, where] {
                auto drawable = factory_.get( name );

                SDL_Rect src {};
                SDL_Rect dst {};

                dst.x = where.x;
                dst.y = where.y;

                src.w = dst.w = drawable.width;
                src.h = dst.h = drawable.height;

                CHECK_FUNCTION( SDL_RenderCopy,
                                canvas_.renderer.get(),
                                drawable.texture.get(),
                                &src,
                                &dst );
            } );
    }

    void Screen::add_draw(Line line, Color color) {
        snapshots_.push_back(
            [this, line, color] {
                set_renderer_color( color );

                CHECK_FUNCTION( SDL_RenderDrawLine,
                                canvas_.renderer.get(),
                                line.from.x, line.from.y,
                                line.to.x, line.to.y );
            }
        );
    }

    void Screen::add_draw(Rect rectangle, Color color, Color_filling filling) {
        snapshots_.push_back(
            [this, rectangle, color, filling] {
                set_renderer_color( color );

                SDL_Rect rect = rectangle;

                CHECK_FUNCTION( SDL_RenderDrawRect,
                                canvas_.renderer.get(),
                                &rect );

                if ( filling == Color_filling::Filled ) {
                    ::SDL_RenderFillRect( canvas_.renderer.get(), &rect );
                }
            }
        );
    }

    void Screen::redraw(Color color) {
        set_renderer_color( color );

        CHECK_FUNCTION( SDL_RenderClear, canvas_.renderer.get() );

        for ( const auto& snapshot : snapshots_ ) {
            snapshot();
        }

        SDL_RenderPresent( canvas_.renderer.get() );
        snapshots_.clear();
    }

    void Screen::set_renderer_color(Color color) {
        CHECK_FUNCTION( SDL_SetRenderDrawColor, canvas_.renderer.get(), color.r, color.g, color.b, color.a );
    }

    Canvas::Canvas(const Screen_properties& properties)
        : window { SDL_CreateWindow( properties.title.c_str(),
                                     properties.position.x,
                                     properties.position.y,
                                     properties.width,
                                     properties.height,
                                     SDL_WINDOW_SHOWN ) },
          renderer { SDL_CreateRenderer( window.get(), -1, 0 ) }
    {
        check_pointer( window );
        check_pointer( renderer );
    }

    void show_message(const Message_content& content, Message_box_type message_box_type) {
        check_function(
            ::SDL_ShowSimpleMessageBox( static_cast<std::uint32_t>( message_box_type ),
                                        content.title.c_str(),
                                        content.text.c_str(),
                                        nullptr )
        );
    }


}
