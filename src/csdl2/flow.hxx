#pragma once

#include <stdexcept>
#include <functional>
#include <chrono>
#include <thread>
#include <atomic>

#include "csdl2/drawing.hxx"


namespace sdl2 {


    using Scancode = SDL_Scancode;
    using Event    = SDL_Event;

    bool key_down(sdl2::Scancode);

    class Main_loop {
     public:
        using Function = std::function<void(void)>;

        explicit Main_loop(const Function&) noexcept;

        void start();

     private:
        Function function_;
    };

    enum class Event_result : bool { Quit, Continue };

    /*!
     *
     */
    class Event_loop {
     public:
        using Event_function         = std::function<Event_result(const sdl2::Event&)>;
        using Unconditional_function = std::function<void(void)>;

        Event_loop(Event_function, Unconditional_function) noexcept;

        void start();

     private:
        Event_function            event_fn_ {};
        Unconditional_function    unconditional_fn_ {};
        Event                     event_ {};
    };

    /*!
     *
     */
    class Thread_manager {
     public:
        using Quit_flag                = std::atomic<bool>;
        using Thread_function          = std::function<void()>;
        using Graphics_init_function   = std::function<Screen()>;
        using Graphics_thread_function = std::function<void(Screen&)>;

        /*!
         *
         */
        void add_graphics_thread(Graphics_init_function, Graphics_thread_function);

        /*!
         *
         */
        void add_thread(Thread_function);

        /*!
         *
         */
        void start_all();

     private:
        Quit_flag                   quit_ { false };
        std::vector<std::thread>    threads_ {};
    };

    /*!
     *
     */
    class Timer {
     public:
        /*!
         *
         */
        explicit Timer(std::chrono::milliseconds) noexcept;

        /*!
         *
         */
        bool ready() noexcept;

        /*!
         *
         */
        std::chrono::milliseconds get_delay() const noexcept;

        /*!
         *
         */
        void set_delay(std::chrono::milliseconds) noexcept;

     private:
        std::uint32_t   delay_ {};
        std::uint32_t   current_ { SDL_GetTicks() };
    };


}
