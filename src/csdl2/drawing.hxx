#pragma once

#include <cstddef>
#include <cstdint>

#include <functional>
#include <string>
#include <vector>
#include <map>

#include <SDL2/SDL.h>

#include "csdl2/resources.hxx"


namespace sdl2 {


    template <class _Numeric>
    struct Basic_point;

    template <class _Integer, class _UInteger>
    struct Basic_rectangle;

    template <class _UInteger>
    struct Basic_size;

    template <class _Numeric>
    struct Basic_rgba_color;

    struct Canvas;
    struct Line;

    using Point        = Basic_point<std::int32_t>;
    using Point_vector = std::vector<Point>;
    using Size         = Basic_size<std::uint32_t>;
    using Rect         = Basic_rectangle<std::int32_t, std::uint32_t>;
    using Color        = Basic_rgba_color<std::uint8_t>;

    template <class _Numeric>
    struct Basic_point {
        _Numeric         x { };
        _Numeric         y { };
    };

    constexpr bool operator ==(const Point& left, const Point& right) noexcept {
        return left.x == right.x && left.y == right.y;
    }

    constexpr bool operator !=(const Point& left, const Point& right) noexcept {
        return left.x != right.x && left.y != right.y;
    }

    constexpr Point& operator +=(Point& left, Point right) noexcept {
        left.x += right.x;
        left.y += right.y;

        return left;
    }

    constexpr Point& operator -=(Point& left, Point right) noexcept{
        left.x -= right.x;
        left.y -= right.y;

        return left;
    }

    constexpr Point& operator *=(Point& left, Point right) noexcept{
        left.x *= right.x;
        left.y *= right.y;

        return left;
    }

    constexpr Point operator +(const Point& left, const Point& right) noexcept {
        return { left.x + right.x, left.y + right.y };
    }

    constexpr Point operator -(const Point& left, const Point& right) noexcept {
        return { left.x - right.x, left.y - right.y };
    }

    constexpr Point operator *(const Point& left, const Point& right) noexcept {
        return { left.x * right.x, left.y * right.y };
    }

    template <class _UInteger>
    struct Basic_size {
        _UInteger   width { };
        _UInteger   height { };
    };

    template <>
    struct Basic_rectangle<std::int32_t, std::uint32_t> {
        std::int32_t    x { };
        std::int32_t    y { };

        std::uint32_t   w { };
        std::uint32_t   h { };

        operator SDL_Rect() const noexcept {
            return { x, y, static_cast<std::int32_t>( w ), static_cast<std::int32_t>( h ) };
        }
    };

    template <class _Integer, class _UInteger>
    struct Basic_rectangle {
        _Integer    x { };
        _Integer    y { };

        _UInteger   w { };
        _UInteger   h { };
    };

    Rect make_rect(std::int32_t x, std::int32_t y, std::uint32_t width, std::uint32_t height) noexcept;
    Rect make_rect(Point pos, std::uint32_t w, std::uint32_t h) noexcept;
    Rect make_rect(Point pos, Size size) noexcept;

    template <class _Numeric>
    struct Basic_rgba_color {
        _Numeric         r { };
        _Numeric         g { };
        _Numeric         b { };
        _Numeric         a { };
    };

    constexpr Color color_red(std::uint8_t amount = 255, std::uint8_t alpha = 255) noexcept;

    constexpr Color color_green(std::uint8_t amount = 255, std::uint8_t alpha = 255) noexcept;

    constexpr Color color_blue(std::uint8_t amount = 255, std::uint8_t alpha = 255) noexcept;

    constexpr Color color_yellow(std::uint8_t amount = 255, std::uint8_t alpha = 255) noexcept;

    constexpr Color color_black(std::uint8_t alpha = 255) noexcept;

    constexpr Color color_white(std::uint8_t amount = 255, std::uint8_t alpha = 255) noexcept;

    struct Screen_properties {
        std::int32_t width  { 800 };
        std::int32_t height { 600 };

        Point        position { SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED };

        std::string  title {};
    };

    struct Canvas {
        Canvas() = default;

        explicit Canvas(const Screen_properties&);

        Unique_window   window { nullptr };
        Unique_renderer renderer { nullptr };
    };

    struct Angle_radian {
        double value {};
    };

    struct Line {
        Line() = default;
        Line(Point, Angle_radian, std::int32_t) noexcept;
        Line(Point, Point) noexcept;

        Point from {};
        Point to {};
    };

    struct Drawable_texture {
        Shared_texture texture { nullptr };
        std::int32_t   width {};
        std::int32_t   height {};
    };

    class Texture_factory {
     public:
        /*!
         *
         */
        void add(const std::string&, Unique_renderer&);

        /*!
         *
         */
        bool exists(const std::string&) const noexcept;

        /*!
         *
         */
        Drawable_texture get(const std::string&) const noexcept;

     private:
        std::map<std::string, Drawable_texture> texture_map_ {};
    };

    enum class Color_filling : bool { Filled, None };

    class Screen {
     public:
        /*!
         *
         */
        explicit Screen(const Screen_properties&);

        /*!
         *
         */
        void add_draw(const std::string&, Point);

        /*!
         *
         */
        void add_draw(Line, Color);

        /*!
         *
         */
        void add_draw(Rect, Color, Color_filling);

        /*!
         *
         */
        void redraw(Color);

     private:
        using Snapshot = std::function<void(void)>;

        /*!
         *
         */
        void set_renderer_color(Color);

        // /*!
        //  *
        //  */
        // void redraw_textures();

        // /*!
        //  *
        //  */
        // void redraw_lines();

        // /*!
        //  *
        //  */
        // struct Texture_to_draw {
        //     Drawable_texture    drawable {};
        //     Point               point;
        // };

        // /*!
        //  *
        //  */
        // struct Line_to_draw {
        //     Line             line {};
        //     Color            color {};
        // };

        Canvas                        canvas_ {};
        Texture_factory               factory_ {};
        std::vector<Snapshot>         snapshots_ {};
        // std::vector<Texture_to_draw>  textures_ {};
        // std::vector<Line_to_draw>     lines_ {};
    };

    enum class Message_box_type : std::uint32_t {
        Basic   = 0,
        Error   = SDL_MESSAGEBOX_ERROR,
        Warning = SDL_MESSAGEBOX_WARNING,
        Info    = SDL_MESSAGEBOX_INFORMATION
    };

    struct Message_content {
        std::string  title {};
        std::string  text {};
    };

    void show_message(const Message_content&, Message_box_type);

    constexpr Color color_red(std::uint8_t amount, std::uint8_t alpha) noexcept {
        return { amount, 0, 0, alpha };
    }

    constexpr Color color_green(std::uint8_t amount, std::uint8_t alpha) noexcept{
        return { 0, amount, 0, alpha };
    }

    constexpr Color color_blue(std::uint8_t amount, std::uint8_t alpha) noexcept {
        return { 0, 0, amount, alpha };
    }

    constexpr Color color_yellow(std::uint8_t amount, std::uint8_t alpha) noexcept {
        return { amount, amount, 0, alpha };
    }

    constexpr Color color_black(std::uint8_t alpha) noexcept {
        return { 0, 0, 0, alpha };
    }

    constexpr Color color_white(std::uint8_t amount, std::uint8_t alpha) noexcept {
        return { amount, amount, amount, alpha };
    }


}
