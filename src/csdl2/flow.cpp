#include "csdl2/flow.hxx"


namespace sdl2 {


    bool key_down(sdl2::Scancode scan_code) {
        return ::SDL_GetKeyboardState( nullptr )[scan_code];
    }

    Main_loop::Main_loop(const Function& a_function) noexcept
        : function_(a_function) {
    }

    void Main_loop::start() {
        while ( !SDL_QuitRequested() ) {
            function_();
        }
    }

    Event_loop::Event_loop(Event_function event_fn, Unconditional_function unconditional_fn) noexcept
        : event_fn_ {event_fn}, unconditional_fn_ {unconditional_fn}
    {
    }

    void Event_loop::start() {
        while ( true ) {
            while ( ::SDL_PollEvent( &event_ ) ) {
                if ( event_fn_( event_ ) == Event_result::Quit ) {

                    return ;
                }
            }

            unconditional_fn_();
        }
    }

    void Thread_manager::add_graphics_thread(Graphics_init_function init, Graphics_thread_function fn) {
        threads_.push_back(
            std::thread(
                [this, init, fn] {
                    auto screen = init();

                    while ( !quit_ ) {
                        ::SDL_PumpEvents();

                        fn( screen );

                        if ( SDL_QuitRequested() ) {
                            quit_ = true;
                        }
                    }
                } ) );
    }

    void Thread_manager::add_thread(Thread_function fn) {
        threads_.push_back(
            std::thread(
                [fn, this] {
                    while ( !quit_ ) {
                        fn();
                    }
                }
            )
        );
    }

    void Thread_manager::start_all() {
        for ( auto & thread : threads_ ) {
            thread.join();
        }
    }

    Timer::Timer(std::chrono::milliseconds delay) noexcept {
        set_delay( delay );
    }

    bool Timer::ready() noexcept {
        auto ticks = ::SDL_GetTicks();

        if ( ticks >= delay_ + current_ ) {
            current_ = ticks;

            return true;
        }

        return false;
    }

    std::chrono::milliseconds Timer::get_delay() const noexcept {
        return std::chrono::milliseconds( delay_ );
    }

    void Timer::set_delay(std::chrono::milliseconds delay) noexcept {
        delay_ = static_cast<std::uint32_t>( delay.count() );
    }


}
