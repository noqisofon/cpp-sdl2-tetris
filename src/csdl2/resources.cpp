#include "csdl2/resources.hxx"


namespace sdl2 {


    Sdl_exception::Sdl_exception()
        : message_ { ::SDL_GetError() }
    {
    }

    const char *Sdl_exception::what() const noexcept {
        return message_.c_str();
    }

    Sdl_system_control::Sdl_system_control(std::uint32_t init_flags) {
        if ( ::SDL_Init( init_flags ) != 0 ) {
            throw Sdl_exception {};
        }
    }

    Sdl_system_control::~Sdl_system_control() noexcept {
        ::SDL_Quit();
    }

    void check_function(std::int32_t function_result) {
        if ( function_result != 0 ) {
            throw Sdl_exception {};
        }
    }

    void Window_deleter::operator() (SDL_Window *window) const noexcept {
        ::SDL_DestroyWindow( window );
    }

    void Renderer_deleter::operator() (SDL_Renderer *renderer) const noexcept {
        ::SDL_DestroyRenderer( renderer );
    }

    void Surface_deleter::operator() (SDL_Surface *surface) const noexcept {
        ::SDL_FreeSurface( surface );
    }

    void Texture_deleter::operator() (SDL_Texture *texture) const noexcept {
        ::SDL_DestroyTexture( texture );
    }

    Shared_texture load_texture(const std::string& name, Unique_renderer& read_point_pointerer) {
        auto surface = Unique_surface { ::SDL_LoadBMP( name.c_str() ) };

        check_pointer( surface );

        auto result = Shared_texture {
            ::SDL_CreateTextureFromSurface( read_point_pointerer.get(), surface.get() ),
            Texture_deleter {}
        };

        check_pointer( result );

        return result;
    }


}
