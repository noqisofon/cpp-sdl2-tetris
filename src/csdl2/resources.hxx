#pragma once

#include <memory>
#include <stdexcept>

#include <SDL2/SDL.h>


namespace sdl2 {


    /*!
     *
     */
    class Sdl_exception : public std::exception {
     public:
        Sdl_exception();

        const char *what() const noexcept override;

     private:
        std::string message_ {};
    };

    template <class _Type>
    void check_pointer(const _Type&);

    void check_function(std::int32_t);

    /*!
     *
     */
    class Sdl_system_control {
     public:
        /*!
         *
         */
        Sdl_system_control(std::uint32_t = SDL_INIT_EVERYTHING);

        /*!
         *
         */
        ~Sdl_system_control() noexcept;

        Sdl_system_control(const Sdl_system_control&)               = delete;
        Sdl_system_control(const Sdl_system_control&&)              = delete;
        Sdl_system_control& operator = (const Sdl_system_control&)  = delete;
        Sdl_system_control& operator = (const Sdl_system_control&&) = delete;
    };

    struct Window_deleter {
        void operator()(SDL_Window *) const noexcept;
    };

    struct Renderer_deleter {
        void operator()(SDL_Renderer *) const noexcept;
    };

    struct Surface_deleter {
        void operator()(SDL_Surface *) const noexcept;
    };

    struct Texture_deleter {
        void operator ()(SDL_Texture *) const noexcept;
    };

    using Unique_window   = std::unique_ptr<SDL_Window, Window_deleter>;
    using Unique_renderer = std::unique_ptr<SDL_Renderer, Renderer_deleter>;
    using Unique_surface  = std::unique_ptr<SDL_Surface, Surface_deleter>;

    using Shared_texture  = std::shared_ptr<SDL_Texture>;

    /*!
     *
     */
    Shared_texture load_texture(const std::string&, Unique_renderer&);

    template <class _Type>
    void check_pointer(const _Type& ptr) {
        if ( ptr == nullptr ) {
            throw Sdl_exception {};
        }
    }


}
