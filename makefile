
.PHONY: all
all: run

.PHONY: run
run: compile
	LD_LIBRARY_PATH=./bin ./bin/sdl2-tetris

./bin/sdl2-tetris:
	make -C ./src/tetris compile

./bin/libcsdl2.so:
	make -C ./src/csdl2  compile

.PHONY: compile
compile:
	make -C ./src/ compile

.PHONY: clean
clean:
	make -C ./src/ clean
